import * as React from "react";

export function App() {
  return (
    <main>
      <h1>Welcome to React</h1>
      <p>Learn how to configure and use JSX</p>
    </main>
  );
}
