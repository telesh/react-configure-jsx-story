# ReactJS: Configure JSX

**To read**: TODO

**Estimated reading time**: TODO

## Story Outline

This story teaches to configure a JSX for a new ReactJS project.

## Story Organization

**Story Branch**: master

> `git checkout master`

**Practical task tag for self-study**: task

> `git checkout task`

Tags: #react, #jsx
